import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs

def partition(X, centers):
    distance_matrix = np.empty((X.shape[0],centers.shape[0]))
    for row_x in range(X.shape[0]):
        for row_center in range(centers.shape[0]):
            diff_vector = centers[row_center,:]-X[row_x,:]
            distance_matrix[row_x,row_center] = diff_vector.dot(diff_vector)
    partition_positions = np.argmin(distance_matrix, axis=1)
    return partition_positions



def update_center(X, partitions, centers):
    for i in range(n_centers):
        closest_centers = np.where(partitions==i)[0]
        if closest_centers.size:
            centers[i,:] = X[closest_centers,:].mean(axis=0)
    return centers


X,y = make_blobs(n_samples=1000, n_features=2, centers=10)


n_centers = 8
centers = np.random.random((n_centers,X.shape[1]))


fig = plt.figure()
ax = fig.add_subplot(111)


paths_x = []
paths_center = []
colors = []
partitions = partition(X, centers)
for i in range(n_centers):
    closest_centers = np.where(partitions==i)[0]
    path_x = ax.scatter(X[closest_centers,0],X[closest_centers,1])
    colors.append(path_x.get_facecolor())
    path_center = ax.scatter(centers[:,0], centers[:,1],marker='v',s=300, edgecolors='k', facecolors=colors[i])
    path_center.set_facecolor(colors[i])
    paths_x.append(path_x)
    paths_center.append(path_center)
plt.pause(2)
plt.draw()

for i in range(100):
    partitions = partition(X, centers)
    for j,path_x in enumerate(paths_x):
        closest_centers = np.where(partitions==j)[0]
        path_x.set_offsets(X[closest_centers,:])
        path_x.set_color(colors[j])
        paths_center[j].set_offsets(centers[j])
        paths_center[j].set_facecolor(colors[j])
    plt.pause(2)
    plt.draw()
    centers = update_center(X, partitions, centers)

